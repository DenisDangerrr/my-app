import React from 'react';
import './App.css';
import HeaderBlock from "./HeaderBlock";
import ContentBlock from "./ContentBlock";
import Header from "./Header";
import Paragraph from "./Paragraph";
import Card from "./Card";

export const wordsList = [
    {
        eng: 'between',
        rus: 'между'
    },
    {
        eng: 'high',
        rus: 'высокий'
    },
    {
        eng: 'really',
        rus: 'действительно'
    },
    {
        eng: 'something',
        rus: 'что-нибудь'
    },
    {
        eng: 'most',
        rus: 'большинство'
    },
    {
        eng: 'another',
        rus: 'другой'
    },
    {
        eng: 'much',
        rus: 'много'
    },
    {
        eng: 'family',
        rus: 'семья'
    },
    {
        eng: 'own',
        rus: 'личный'
    },
    {
        eng: 'out',
        rus: 'из/вне'
    },
    {
        eng: 'leave',
        rus: 'покидать'
    },
    {
        eng: 'feel',
        rus: 'чувствовать',
    },
];


const App = () => {
    return(
       <>
        <HeaderBlock
            title='Учите слова онлайн!'
            descr='Воспользуйтесь карточками для запоминания и пополнения активныйх словарных
                    запасов.'
        />

       <HeaderBlock HideBackground>
           <Header>
               Учите слова онлайн!
           </Header>
           <Paragraph>
               Воспользуйтесь карточками для запоминания и пополнения активныйх словарных
               запасов.
           </Paragraph>
           <Paragraph>
               Воспользуйтесь карточками для запоминания и пополнения активныйх словарных
               запасов.
           </Paragraph>
           <Paragraph>
               Воспользуйтесь карточками для запоминания и пополнения активныйх словарных
               запасов.
           </Paragraph>
       </HeaderBlock>

        <div>
            {
                wordsList.map(({eng, rus}, index) =>
                    <Card key={index} eng={eng} rus={rus} />
                    )
            }
        </div>

        <ContentBlock
            title='Начните общение в живую!'
            par='Обще́ние — сложный многоплановый процесс установления и развития контактов между людьми (межличностное общение) и группами (межгрупповое общение),
             порождаемый потребностями совместной деятельности и включающий в себя как минимум три различных процесса: коммуникацию (обмен информацией), интеракцию (обмен действиями)
              и социальную перцепцию (восприятие и понимание партнера).'
            descr='Язык – это дорожная карта культуры. Он рассказывает, откуда пришел и куда идет его народ. (Рита Мэй Браун)'
        />
       </>
    )

};
export default App;
