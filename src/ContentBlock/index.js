import React from "react";
import s from "./ContentBlock.module.scss"
import {ReactComponent as ReactLogoSvg} from "../logo.svg";

const ContentBlock = ({title, descr, par}) => {
    const placeholder = 'Search new people...';
    return (
        <>
            <div className={s.cover}>
                <div className={s.wrap}>
                    <h2 className={s.title}>{title}</h2>
                    <ReactLogoSvg/>
                    <p className={s.descr}>{descr}</p>
                    <div className={s.picOne}></div>
                    <p className={s.par}>{par}</p>
                    <p className={s.par}>Общение с новыми людьми и обретение новых знакомств - прекраснешее чувство которое есть в жизни человека!</p>
                    <input type="text" placeholder={placeholder} className={s.inputContent}/>
                    <br/>
                    <button className={s.search}>Click me!</button>
                </div>
            </div>
        </>
    )
};

export default ContentBlock;